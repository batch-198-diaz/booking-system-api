const mongoose = require("mongoose");

// Course.js

const courseSchema = new mongoose.Schema({

	name: {
		type:String,
		required: [true,"Name is required"]
	},
	description: {
		type:String,
		required: [true,"Description is required"]
	},
	price: {
		type:Number,
		required: [true,"Price is required"]
	},
	isActive: {
		type:Boolean,
		default: true
	},
	createdOn: {
		type:Date,
		default: new Date()
	},
	enrollees: [

		{
			userId:	{
				type:String,
				required: [true,"User ID is required"]
			},
			dateEnrolled:	{
				type:Date,
				default: new Date()
			},
			status:	{
				type:String,
				default: "Enrolled"
			}
		}
	]

})


module.exports = mongoose.model("Course",courseSchema);

/*
	module.exports
		import and use this file in another file


	Mongoose Model is the connection to our collection.

		2 arguments:
		 - First, the name of the collection the model is going to connect to. In mongoDB, once we create a new course document this model will connect to our courses collection. But since the courses collection is not yet created initially, mongodb will create it for us.
		 - Second, the schema of the documents in the collection.

*/