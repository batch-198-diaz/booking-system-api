// index.js


const express = require("express");

const mongoose = require("mongoose");
//Mongoose - ODM lib to let ExpressJS API manipulate a MongoDB DB

const app = express();
const port = 4000;

/*

mongoose.connect() 
	- connects our api to mongodb database via the use of mongoose 

It has 2 arguments 

	1. the connection string 
		(fetched from MongoDB > Database > Cluster 0 > Connect > Connect your Application > Copy Connection String) and then input password by replacing "<password>" and just before ?, add the database name (bookingAPI, in this case)

	2. an object 
		used to add info between mongoose and mongodb

mongoose.connection 
	- to create notif if db connection is successess/failed
	- it shows a notif of an internal server error
	- open and successful connection, show output message in terminal

express.json() 
	- line 46 - to handle request body and parse it into JS objects

*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.xxn22.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection;
db.on('error', console.error.bind(console, "MongoDB Connection Error."));
db.once('open',()=>console.log("Connected to MongoDB"));

app.use(express.json());

const courseRoutes = require('./routes/courseRoutes');
app.use('/courses',courseRoutes);


const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

app.listen(port,()=>console.log("ExpressJS API running at localhost:4000"))