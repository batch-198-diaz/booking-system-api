// userRoutes.js


/*The Route should just contain the endpoints and should only trigger the function but it shouldn't be where we define the logic of the function. Now instead of the logic we replace it with a HANDLER FUNCTION that will be located in a separate file called Controllers.

	Updated Route Syntax:
	router.method("/endpoint",handlerFunction)

		with Middleware:
		router.method("/endpoint",middleware,handlerFunction)	
*/

// IMPORTS

const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
//console.log(userControllers); 

	/* 

	auth - to access and use the verify methods to act as middleware for our routes. middlewares
	
	const {verify} - verify inside curly brackets to destructure auth to get only our methods and save it in variables

	notes: will do login to get token and register doesn't need token

	*/

const auth = require("../auth");
const {verify} = auth;

// ROUTES

router.post("/",userControllers.registerUser);

router.post("/details",verify,userControllers.getUserDetails);

router.post("/checkEmail",userControllers.getUserDetailsbyEmail);

	// User Authentication
router.post("/login",userControllers.loginUser);

	// Enroll
router.post("/enroll",verify,userControllers.enroll);


module.exports = router;




//====> MOVED TO userControllers.js

/*	
	const bcrypt = require("bcrypt"); //hash passwords
	const User = require("../models/User")
*/

/*
(req,res)=>{

	console.log(req.body) 
	to check the input passed via the client

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		//password: req.body.password,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
})
*/

/*
(req,res)=>{
	
	User.findByOne({_id:req.body.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))

})
*/
