// courseRoutes.js

/*
	This is to create routes from another file to be used in our application. From here, we have to import express as well.

	The Router() method is used to contain routes
		- instead of using express() which is in variable "app" we will now use this method for get and post methods in the example below thus, we replace it with router.get and router.post.

	module.exports
		add this again to import and use this file in another file

	Using the Course model to connect to our collection and retrieve our courses. We use this to query into our collection.

	Added verifyadmin that always comes after verify.

	**Route Parameter with "get" method is used to directly pass data through the URL instead of using "post" method and having a request body to input id. syntax is /endpoint/:routeParams -- update for activity s36-37 for /courseDetails



*/

const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth")
const {verify,verifyAdmin} = auth;


router.get("/",verify,verifyAdmin,courseControllers.getAllCourses);

router.post("/",verify,verifyAdmin,courseControllers.addCourse);

router.get("/activeCourses",courseControllers.getActiveCourses);

// router.post("/courseDetails",courseControllers.getSingleCourse); -- change/update**
router.get("/courseDetails/:courseId",courseControllers.getSingleCourse);


//UPDATE
router.put("/updateCourse/:courseId",verify,verifyAdmin,courseControllers.updateCourse);

//ARCHIVE
router.delete("/archiveCourse/:courseId",verify,verifyAdmin,courseControllers.archiveCourse)


module.exports = router;


// ====> MOVED TO courseControllers.js

/*

const Course = require("../models/Course");


(req,res)=>{

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

});


(req,res)=>{

	// res.send("This route will create a new course document.");
	// console.log(req.body)

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

});

*/