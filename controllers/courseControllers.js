//courseControllers.js

const Course = require("../models/Course");

module.exports.getAllCourses = (req,res)=>{

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

module.exports.addCourse = (req,res)=>{

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

module.exports.getActiveCourses = (req,res)=>{

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

/* ======== ACTIVITY s36-s37======== */

module.exports.getSingleCourse = (req,res)=>{

	console.log(req.params.courseId) 

	// Course.findById(req.body.id) -- changed/updated to partner the route parameter**
	Course.findById(req.params.courseId)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

/* ===== END OF ACTIVITY s36-s37===== */


// UPDATE - PUT
module.exports.updateCourse = (req,res)=>{

	// console.log(req.params.courseId);
	// console.log(req.body);

	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


// ARCHIVE - DELETE
module.exports.archiveCourse = (req,res) =>{

	console.log(req.params.courseId);

	let update = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

/*

	**req.params is an object that contains the value captured via route params and the findById is changed from req.body.id to req.params.<name_of_route_parameter> because we don't get any data from the request body anymore but from the URL directly

	findByIdAndUpdate - used to find specific id then update the document with a given "update" object that can be either defined separately with let or directly inserted into the syntax. let update = {insert_object} OR model.findByIdAndUpdate(<route_param_id>,{insert_object},{new:true})


	.then() process the result of a previous function/method in its own

	.catch() catches the errors and allows to process and send to the client



*/