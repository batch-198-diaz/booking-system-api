/*

	userControllers.js - naming convention is based on its model
	
	CONTROLLERS 
		- Business logic of our API should be here.
		- Triggered by a route	
*/

/*
	-import the User Model here in the controllers 
	-import bcrypt as well because it is used here
	- registerUser, getUserDetails and loginUser
		are methods and are anonymous functions in an object

	- User.findById(_id:req.body.id) is changed to req.user.id to ensure that the logged in user or user that passed the token will only be able to get their own details and only theirs.

*/

const User = require("../models/User");
const Course = require("../models/Course");

const auth = require("../auth");
const bcrypt = require("bcrypt");

module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	// console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo
	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.getUserDetails = (req,res)=>{

	// console.log(req.body);
	// console.log(req.user);

	// User.findById(_id:req.body.id)
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

/* ======== ACTIVITY s36-s37======== */

module.exports.getUserDetailsbyEmail = (req,res)=>{

	User.findOne({email:req.body.email})
	// .then(result => res.send(result)) -- change/update**
	.then(result => {

		if(result === null){
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(error => res.send(error))

}
// **findOne will return null if no match is found so we only need to send true or false whether the email exists or not

/* ===== END OF ACTIVITY s36-s37===== */



module.exports.loginUser = (req,res)=>{

	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send({message: "No User Found."})
		} else {
			// console.log(foundUser)
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			//console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				// console.log("We will create a token for the user if the password is correct")
				return res.send({accessToken: auth.createAccessToken(foundUser)});

			} else {
				return res.send({message: "Incorrect Password"});
			}

		}
	})

}


module.exports.enroll = async (req,res) =>{

	// console.log(req.user.id);
	// console.log(req.body.courseId);

	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden."});
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		// console.log(user);

		let newEnrollment = {
			courseId: req.body.courseId
			// courseId: {}
		}

		user.enrollments.push(newEnrollment);

		return user.save().then(user => true).catch(err => err.message);

	})

	// console.log(isUserUpdated);
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		// console.log(course)

		let newEnrollee = {
			userId: req.user.id
		}

		course.enrollees.push(newEnrollee);

		return course.save().then(course => true).catch(err => err.message);
	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	//both are true - successful enrollment
	if(isUserUpdated && isCourseUpdated) {
        return res.send({message:"Thank you for enrolling!"})
    }

}

	/* User Enrollment

		Steps:
		1. find the user who is enrolling (need to verify so token is needed)
		2. update the enrollment subdocument array by pushing courseId in it
		3. find the course that will be enrolled to
		4. update the enrollees subdocument array by pushing userId in it

		5. validate if the user is an admin, if yes no access, else contine
		
		6. isUserUpdated 
			- to determine the result of the query and if we were able to save the courseId into our user's enrollment subdocument array, true if success else false
			- newEnrollment is to be created and to be pushed into the enrollments subdocument array of user as a new enrollment ex. push(newEnrollment)
			- save user document and return true if pushed(or saved) successfully else return the error message
			- check if user is updated, if not send error message in client
		
		7. isCourseUpdated 
			- if user added as enrollee in course, true if success else false
			- newEnrollee is to be created and to be pushed into the enrollees subdocument array of course as a new enrollee ex. push(newEnrollee)
			- save course document and return true if pushed(or saved) successfully else return the error message
			- check if course is updated, if not send error message in client

		8. 

		Notes:

		- courseId will come from the req.body
		- userId will come from the req.user because we used verify, reference in auth.js
		- since two collections will be accessed with one action, we'll have to wait for the completion of the action instead of letting Javascript continue line per line

			- async 
				- added to make a function asynchronous ex. async (req,res) =>{}
				- instead of JS regular behaviour of running each code line by line, it will be able to wait for the result of a function
			- await 
				- to be able to wait for a result of a function before proceeding
				- can only be used inside a async function

		
	*/


	/* User Login

		Steps:
		1. find the user by their email
		2. check their password if input and hashed password in our db is a match
		3. if not found, send a message to client "not found or wrong pass"
		4. if user input and hasded pass is a match, then generate a "key" for the user to have authorization to access certain features in our app. 
		5. auth.js is used to create a key/token to authorize the user. this module will create an encoded string which contains our user's details. 
		6. JSONWebToken 

		Notes:

		- foundUser is the parameter that contains the result of findOne
		- findOne() returns null if it is unable to find a match
		- res.send({message:"message"}) is an object with our message that the client(postman) will receive if no user was found
		- in else statement, if we find a user, foundUser will contain the document that matched the input email
		- .compareSync() checks if input password from req.body matches the hashed password in our foundUser document

		- auth.createAccessToken receives our foundUser document as an argument, gets only necessary details, wraps those details in JWT and the secret, then finally returns our JWT which will be sent to the client.
	*/

/* 
	SOLUTION (s35-ACTIVITY: Detail Route) - now called getUserDetails

	// find() returns an array even if the result is only one

	User.find({_id:req.body.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))

	OR use findOne() and returns 1 docu

	User.findOne({_id:req.body.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))

	OR use findById

	User.findById({req.body.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))

*/


