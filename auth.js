/* 

auth.js 

	is our own module which contains methods to help authorize or restrict users from accessing certain features in our application.

	The secret string (i.e. "courseBookingAPI" in this example) will be used to check the validity of a passed token. IF a token does not contain this secret string, then that token is invalid or illegitimate.

JWT 
		- is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access certain parts of our app.
		- it will encode the user's details and can only be decoded by jwt's own methods where the secret is kept intact. If the jwt seemed tampered with, then it will reject the user's attempt to access a feature in the app.
		
		jwt.sign() will create a JWT using our data object with secret and then the empty {} is for the default algorithm 

VERIFY

	- verify() used as middleware wherein it will be added per route to act as a gate to check if the token being passed is valid or not or if the user is allowed to access certain features or not.

	- req.headers.authorizatoin is where we will pass the token with our request headers as authorization. Requests that need a token must be able to pass the token in the authorization headers.

	- token undefined - then req.headers.authorization is empty which means the request did not pass a token in the authorization headers

TOKEN 

	- when passing JWT we use the Bearer Token authorization which means JWT passes a word "Bearer" with the token separated with space. Token variable should be updated with the sliced version.

	ex: 
	Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyZTg5MDhiZjExZTE1NzcyZDg5MzgyNSIsImVtYWlsIjoic2FtcGxlQGdtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NTk0MTMwMzF9.EhMukoF5tkHyQFndwvWNTofkxo9zL9F204eFOfP50w0

	- token.slice() and copy the rest of the token without the word 'Bearer'
		slice(<startingPosition>,<endPosition>)

	- jwt.verify() to check overall length and if secret is included. it has 3 arguments which is the token, secret, then the handler function which will handle (or hold/contain?) either:

		- the error message if token is invalid or 
		- the decoded data from the token if it checks out.

		req.user = decodedToken 
			to add a new user property in the request object and add the decoded token as its value. therefore, the next controller or middleware will now have access to the id, email and isAdmin properties of the logged in user

		next() enables to proceed to the next middleware or controller

	- verify() would not only check validity but add decoded data of token in the request object as req.user (given in the example)

VERIFYADMIN

	added as middleware and has to be added after verify() to check validity and add decodedToken in the request object as req.user

	in Routes, verifyAdmin must come after verify to have access to req.user. it will disallow regular logged in and non-logged in users from using the route.

*/


// notes: auth and JWT
const jwt = require("jsonwebtoken");

const secret = "courseBookingAPI"

module.exports.createAccessToken = (userDetails) => {

	console.log(userDetails);

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}

	console.log(data);

	return jwt.sign(data,secret,{});

}


// notes: verify and token
module.exports.verify = (req,res,next) => {

	let token = req.headers.authorization

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."});
	} else {

		// console.log(token);

		token = token.slice(7);

		// console.log(token);

		jwt.verify(token,secret,function(err,decodedToken){

			// console.log(decodedToken);
			// console.log(err);

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else {
				req.user = decodedToken;
				next();
			}
		})
	}
}


module.exports.verifyAdmin = (req,res,next) => {
	console.log(req.user);

	if(req.user.isAdmin){
		next();
	} else {
		return res.send({

			auth: "Failed",
			message: "Action Forbidden"

		})
	}
}

